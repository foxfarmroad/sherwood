# Robinhood's Playground

### 1. DynamoDB table for storage of stock history

### Setup
#### 1. npm i serverless -g
#### 3. npm i serverless-dynamodb-autoscaling

### Deploy
#### There's an open issue that is blocking using profiles that assume roles: https://github.com/serverless/serverless/issues/5048
#### sls deploy --stage dev --force --verbose

### Debugging
#### Export SLS_DEBUG=* environment variable to view all Serverless debug logs