var AWS = require('aws-sdk');

function putSsmParameter (ssm, params) {
  params.forEach(param => {
    ssm.putParameter(param, function(err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
      } else {
        console.log(data);           // successful response
      }
    });
  });
}

function handler (data, serverless, options) {
  AWS.config.update({region: serverless.service.provider.region});
  var ssm = new AWS.SSM();

  var params = [];

  for (var key in data) {
    params.push(
      {
        Name: `/${serverless.service.provider.stage}/${key}`, /* required */
        Type: 'String', /* required */
        Value: data[key], /* required */
        Overwrite: true
      }
    );
  }

  putSsmParameter(ssm, params);
}
 
module.exports = { handler }